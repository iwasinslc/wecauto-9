<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Wallet",
 *     description="Wallet Model",
 *     @OA\Xml(
 *         name="Wallet"
 *     )
 * )
 */
class Wallet
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="Wallet ID",
     *     type="string",
     *     example="071e8ce0-06d7-11eb-bf15-39f82180b0c0"
     * )
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *     title="Balance",
     *     description="Amout of balance for the wallet",
     *     type="string",
     *     example=1000
     * )
     * @var float
     */
    public $balance;

    /**
     * @OA\Property(
     *     title="Currency",
     *     description="Currency code",
     *     type="string",
     *     example="WEC"
     * )
     * @var string
     */
    public $currency;

    /**
     * @OA\Property(
     *     title="Payment System",
     *     description="Payment System Name which is connected to wallet",
     *     type="string",
     *     example="Webcoinapi"
     * )
     * @var string
     */
    public $payment_system;

    /**
     *     @OA\Property(
     *         title="Address",
     *         description="The address of the wallet",
     *         type="string",
     *         example="1BvBMSEYstWetqTFn5Au4m4GFg7xJaNVN2"
     *     )
     * @var string
     */
    public $address;
}