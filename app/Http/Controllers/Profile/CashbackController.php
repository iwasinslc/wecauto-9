<?php


namespace App\Http\Controllers\Profile;


use App\Http\Controllers\Controller;
use App\Http\Requests\RequestCashbackRequestStore;
use App\Models\CashbackRequest;
use Yajra\DataTables\DataTables;

class CashbackController extends Controller
{
    public function __construct()
    {

    }

    /**
     * Show cashback list page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('profile.cashback.index');
    }

    /**
     * Get datatable
     * @return mixed
     * @throws \Exception
     */
    public function dataTable()
    {
        $requests = CashbackRequest::query()
            ->where('user_id', user()->id)
            ->get();

        return DataTables::of($requests)
            ->addIndexColumn()
            ->addColumn('status', function($data) {
                return $data->approved === null
                    ? 'На рассмотрении'
                    : ($data->approved ? 'Одобрена' : 'Отклонена');
            })
            ->make(true);
    }

    /**
     * @param $cashbackRequestId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show($cashbackRequestId)
    {
        /** @var CashbackRequest $cashbackRequest */
        $cashbackRequest = CashbackRequest::query()
            ->where('user_id', user()->id)
            ->with('documents')
            ->find($cashbackRequestId);

        if (!$cashbackRequest) {
            return redirect(route('profile.cashback.index'))
                ->with('error', 'Заяка не найдена');
        }

        $documents = [];
        foreach($cashbackRequest->getMedia('cashback_request') as $media) {
            $documents[] = [
                'name' => $media->name,
                'url' => config('app.env') == 'develop'
                    ? $media->getFullUrl()
                    : $media->getUrl()
            ];
        }

        return view('profile.cashback.show', compact('cashbackRequest', 'documents'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->middleware('has_closed_deposits');

        return view('profile.cashback.create', [
            'user' => user()
        ]);
    }

    /**
     * Store cashback request
     * @param RequestCashbackRequestStore $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws
     */
    public function store(RequestCashbackRequestStore $request)
    {
        $this->middleware('has_closed_deposits');
        $user = user();
        if (isCashbackRequestSent()) {
            return back()->with('error', __('Заявка уже была оформлена'));
        }

        $validated = $request->only(['telegram', 'video_link', 'comment']);
        try {
            \DB::transaction(function () use ($user, $validated, $request) {
                /** @var CashbackRequest $cashbackRequest */
                $cashbackRequest = CashbackRequest::create(array_merge([
                    'user_id' => $user->id,
                ], $validated));

                if ($request->hasFile('files')) {
                    foreach($request->file('files') as $file)
                    {
                        $cashbackRequest->addMedia($file)
                            ->addCustomHeaders([
                                'ACL' => 'public-read'
                            ])
                            ->usingFileName(time() . '.' . $file->getClientOriginalExtension())
                            ->toMediaCollection('cashback_request', config('app.env') == 'develop' ? 'public' : 's3');
                    }
                }
            });
            return back()->with('success', __('Заявка оформлена'));
        } catch (\Exception $e) {
            \Log::error('Error of loading files', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);
            return back()->with('error', __('Заявка не может быть оформлена'))->withInput();
        }

    }

    /**
     * Show conditions page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function conditions()
    {
        return view('profile.cashback.conditions');
    }
}