<?php
namespace App\Rules;

use App\Models\Currency;
use App\Models\ExchangeOrder;
use App\Models\Rate;
use App\Models\Wallet;
use Illuminate\Contracts\Validation\Rule;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RuleExchangeEnoughBalance
 * @package App\Rules
 */
class RuleExchangeEnoughBalance implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user   = user();
        /**
         * @var Wallet $mainWallet
         */
        $mainWallet = $user->wallets()->find(request()->main_wallet_id);
        /**
         * @var Wallet $wallet
         */
        $wallet = $user->wallets()->find(request()->wallet_id);


        $user_wallet = request()->type==ExchangeOrder::TYPE_SELL ?  $mainWallet : $wallet;

        $amount = request()->amount;

        $amount = request()->type==ExchangeOrder::TYPE_SELL?  $amount : $amount*request()->rate;

        return $user_wallet ? $user_wallet->balance >= $amount : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.enough_balance');
    }
}
