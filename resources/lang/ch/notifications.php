<?php
return [
    'order_created'       => '订购 #:id :amount :currency 成功创建',
    'order_closed'        => '订购 #:id :amount :currency 由于差额资金不足，已关闭',
    'sale'                => '销售 :amount :currency 成功地通过',
    'purchase'            => '采购 :amount :currency 成功地通过',
    'partner_accrue'      => '您刚才从 :level 级 :login 用户收到了 :amount :currency 的佣金',
    'wallet_refiled'      => '您的钱包已充值 :amount :currency',
    'rejected_withdrawal' => '您的提款价值为 :amount :currency ，已被取消.',
    'approved_withdrawal' => '您的提款价值为 :amount :currency 确认.',
    'new_partner'         => '您在第 :level 级上有一个新的 :login 合作伙伴',
    'parking_bonus'       => '停车奖金 :amount :currency',
    'licence_cash_back'   => '购买许可证的现金返还 :amount :currency'
];