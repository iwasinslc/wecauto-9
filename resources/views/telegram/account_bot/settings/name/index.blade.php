<?php
$user = $telegramUser->user;

if (null == $user) {
    die('ok');
}
?>
@if(!empty($user->name) && $user->name != $telegramUser->telegram_user_id)
😀 <b>{{__('Name')}}:</b> {{ $user->name }}

ℹ️ <i>{{__('To change the name, please, write new one.')}}</i>
@else
😀 <b>{{__('To set the name, just type it as a message.')}}</b>
@endif